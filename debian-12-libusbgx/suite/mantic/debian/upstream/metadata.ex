# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/libusbgx/issues
# Bug-Submit: https://github.com/<user>/libusbgx/issues/new
# Changelog: https://github.com/<user>/libusbgx/blob/master/CHANGES
# Documentation: https://github.com/<user>/libusbgx/wiki
# Repository-Browse: https://github.com/<user>/libusbgx
# Repository: https://github.com/<user>/libusbgx.git
